# Hausaufgabe 4

## Aufgabe 1 - Vektorrechnung (21 Punkte)

### i) Header (3 Punkte)

Deklarieren Sie einen Header vector3D.h, in welchem Sie die Signaturen von zwei Funktionen
für die Addition und Subtraktion von Vektoren definieren.
Die Vektoren sollen durch Arrays der Länge 3 repräsentiert werden.
Die Funktionen sollen jeweils zwei Vektoren als Operanden übergeben bekommen und
einen weiteren Vektor für das Ergebnis der Operation.Warum ist dies nötig? Beantworten
Sie die Frage in einem Kommentar im Quelltext.

### ii) Implementierung (4 Punkte)

Implementieren Sie die in Teil i) definierten Funktionen in einer Datei vector3D.(c|cpp).

### iii) Testprogramm (3 Punkte)

Schreiben Sie in einer weiteren Datei ein Programm mit einer main-Routine, in welcher Sie
die zuvor implementierten Funktionen testen. Lassen Sie dabei den Nutzer zwei Vektoren
eingeben und rufen Sie mit diesen Werten die Funktionen für Addition und Subtraktion
auf. Geben Sie die Ergebnisse auf dem Bildschirm aus.

### iv) Vektoren als Verbund (4 Punkte) Setzen Sie die Teilaufgaben i) bis iii) unter

Verwendung einer Verbundstruktur für die Repräsentation von Vektoren um. Welche
Änderungen an den Funktionen ergeben sich dadurch? Beantworten Sie Frage in einem
Kommentar im Quelltext.

### v) Variable Anzahl von Parametern (4 Punkte)

Überlegen Sie sich eine Möglichkeit, die Parameter der Additionsfunktion dahingehend
anzupassen, dass eine variable Anzahl an Vektoren übergeben werden kann. All diese
Vektoren sollen dann innerhalb der Routine aufaddiert werden. Implementieren Sie diese
Funktion.
