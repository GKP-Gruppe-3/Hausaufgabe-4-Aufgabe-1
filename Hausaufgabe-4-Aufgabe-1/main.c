#include "stdafx.h"

#include "util.h"
#include "vector3D.h"

/******************************************************************************
* Aufgabe 1
******************************************************************************/
/* Testwerte f�r einen Teildurchlauf (mit Zeilenumbr�chen auf die Konsole kopieren):
6
5
4
3
2
1
*/
int main()
{
  /***************************************
  * Aufgabe 1.3 - Testprogramm
  ***************************************/

  vector3Da_t vector3Da_1;
  vector3Da_t vector3Da_2;
  vector3Da_t vector3Da_sum_va;
  vector3Da_t vector3Da_sum;
  vector3Da_t vector3Da_diff;
  vector3DaGetFromConsole(vector3Da_1, "Bitte einen 3D-Vektor (Array) eingeben...\n");
  vector3DaGetFromConsole(vector3Da_2, "Bitte einen weiteren 3D-Vektor (Array) eingeben...\n");
  vector3DaAddVA(vector3Da_sum_va, vector3Da_1, vector3Da_2, NULL);
  vector3DaAdd(vector3Da_sum, vector3Da_1, vector3Da_2);
  vector3DaSubtract(vector3Da_diff, vector3Da_1, vector3Da_2);
  vector3DaAdd(vector3Da_sum, vector3Da_1, vector3Da_2);
  vector3DaPut(vector3Da_1, "Der erste Vektor - ");
  vector3DaPut(vector3Da_2, "Der zweite Vektor - ");
  vector3DaPut(vector3Da_sum_va, "Die Summe der zwei Vektoren (VARARGS) - ");
  vector3DaPut(vector3Da_sum, "Die Summe der zwei Vektoren - ");
  vector3DaPut(vector3Da_diff, "Die Differenz der zwei Vektoren - ");
  pause();

  /***************************************
  * Aufgabe 1.4 - Testprogramm
  ***************************************/

  vector3Ds_t vector3Ds_1 = vector3DsGetFromConsole("Bitte einen 3D-Vektor (Struct) eingeben...\n");
  vector3Ds_t vector3Ds_2 = vector3DsGetFromConsole("Bitte einen weiteren 3D-Vektor (Struct) eingeben...\n");
  vector3Ds_t vector3Ds_sum_va = vector3DsAddVA(&vector3Ds_1, &vector3Ds_2, NULL);
  vector3Ds_t vector3Ds_sum = vector3DsAdd(&vector3Ds_1, &vector3Ds_2);
  vector3Ds_t vector3Ds_diff = vector3DsSubtract(&vector3Ds_1, &vector3Ds_2);
  vector3DsPut(&vector3Ds_1, "Der erste Vektor - ");
  vector3DsPut(&vector3Ds_2, "Der zweite Vektor - ");
  vector3DsPut(&vector3Ds_sum_va, "Die Summe der zwei Vektoren (VARARGS) - ");
  vector3DsPut(&vector3Ds_sum, "Die Summe der zwei Vektoren - ");
  vector3DsPut(&vector3Ds_diff, "Die Differenz der zwei Vektoren - ");
  pause();

  /**************************************/
  return 0;
}
