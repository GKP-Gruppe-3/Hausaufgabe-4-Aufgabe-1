#include "stdafx.h"

#include "vector3D.h"
#include "util.h"

#include <stdarg.h>

/******************************************************************************
* Aufgabe 1.2 - Implementation
******************************************************************************/

// Einen 3D-Vektor (Array) von der Konsole lesen.
void vector3DaGetFromConsole(vector3Da_t result, char *message)
{
  if (message != NULL)
  {
    printf(message);
  }

  result[0] = doubleGetFromConsole("  Der erste Wert des Vektors:  ");
  result[1] = doubleGetFromConsole("  Der zweite Wert des Vektors: ");
  result[2] = doubleGetFromConsole("  Der dritte Wert des Vektors: ");
}

// Einen 3D-Vektor (Array) auf der Konsole ausgeben.
void vector3DaPut(vector3Da_t vector, char *message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  printf("3D-Vektor (Array):\n");
  printf("  Erster Wert:  %lf\n", vector[0]);
  printf("  Zweiter Wert: %lf\n", vector[1]);
  printf("  Dritter Wert: %lf\n", vector[2]);
}

/***************************************
* Aufgabe 1.5 - Array-Vektor
***************************************/
// Mehrere 3D-Vektoren (Array) addieren und das Ergebnis liefern.
void vector3DaAddVA(vector3Da_t result, vector3Da_t operand, ...)
{
  va_list arg_list;
  va_start(arg_list, operand);

  // Der Datentyp "vector3Da_t" ist ein Array - es wird also immer als Adresse �bergeben.
  // Das Funktionsergebnis darf keine lokale Variable sein (keine "Closures" m�glich).
  // Entweder wird ein Array auf dem Heap angelegt, oder der Aufrufer liefert ein Ziel-Array.
  result[0] = operand[0];
  result[1] = operand[1];
  result[2] = operand[2];
  double *other;

  for (;;)
  {
    // Nutzung des Datentyps "vector3Da_t" f�hrte hier zu Problemen,
    // deshalb wird die Pointer-Entsprechung genutzt.
    other = va_arg(arg_list, double*);
    if (other == NULL)
    {
      // Der letzte Wert muss "NULL" sein;
      // so muss das Ende der Argumentenliste gekennzeichnet sein.
      break;
    }
    result[0] += other[0];
    result[1] += other[1];
    result[2] += other[2];
  }

  va_end(arg_list);
}

// Zwei 3D-Vektoren (Array) addieren und das Ergebnis liefern.
void vector3DaAdd(vector3Da_t result, vector3Da_t left, vector3Da_t right)
{
  // Der Datentyp "vector3Da_t" ist ein Array - es wird also immer als Adresse �bergeben.
  // Das Funktionsergebnis darf keine lokale Variable sein (keine "Closures" m�glich).
  // Entweder wird ein Array auf dem Heap angelegt, oder der Aufrufer liefert ein Ziel-Array.
  result[0] = left[0] + right[0];
  result[1] = left[1] + right[1];
  result[2] = left[2] + right[2];
}

// Von einem 3D-Vektor (Array) einen anderen 3D-Vektor (Array) subtrahieren und das Ergebnis liefern.
void vector3DaSubtract(vector3Da_t result, vector3Da_t left, vector3Da_t right)
{
  // Der Datentyp "vector3Da_t" ist ein Array - es wird also immer als Adresse �bergeben.
  // Das Funktionsergebnis darf keine lokale Variable sein (keine "Closures" m�glich).
  // Entweder wird ein Array auf dem Heap angelegt, oder der Aufrufer liefert ein Ziel-Array.
  result[0] = left[0] - right[0];
  result[1] = left[1] - right[1];
  result[2] = left[2] - right[2];
}

/******************************************************************************
* Aufgabe 1.4 - Implementation
******************************************************************************/

// Einen 3D-Vektor (Struct) von der Konsole lesen.
vector3Ds_t vector3DsGetFromConsole(char *message)
{
  if (message != NULL)
  {
    printf(message);
  }

  vector3Ds_t result;
  result.x = doubleGetFromConsole("  x (Der erste Wert des Vektors):  ");
  result.y = doubleGetFromConsole("  y (Der zweite Wert des Vektors): ");
  result.z = doubleGetFromConsole("  z (Der dritte Wert des Vektors): ");

  return  result;
}

// Einen 3D-Vektor (Struct) auf der Konsole ausgeben.
void vector3DsPut(vector3Ds_t *vector, char *message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  printf("3D-Vektor (Struct):\n");
  printf("  x (Erster Wert):  %lf\n", vector->x);
  printf("  y (Zweiter Wert): %lf\n", vector->y);
  printf("  z (Dritter Wert): %lf\n", vector->z);
}

// Mehrere 3D-Vektoren (Struct) addieren und das Ergebnis liefern.
vector3Ds_t vector3DsAddVA(vector3Ds_t *operand, ...)
{
  va_list arg_list;
  va_start(arg_list, operand);

  // Der Datentyp "vector3Da_t" ist ein Struct - es wird also immer als Kopie �bergeben.
  // Im Gegensatz zu einem Array ist die direkte R�ckgabe eines Struct kein Problem.
  vector3Ds_t result = *operand;
  vector3Ds_t *other;

  for (;;)
  {
    other = va_arg(arg_list, vector3Ds_t*);
    if (other == NULL)
    {
      // Der letzte Wert muss "NULL" sein;
      // so muss das Ende der Argumentenliste gekennzeichnet sein.
      break;
    }
    result.x += other->x;
    result.y += other->y;
    result.z += other->z;
  }

  va_end(arg_list);
  return result;
}

// Zwei 3D-Vektoren (Struct) addieren und das Ergebnis liefern.
vector3Ds_t vector3DsAdd(vector3Ds_t *left, vector3Ds_t *right)
{
  // Der Datentyp "vector3Da_t" ist ein Struct - es wird also immer als Kopie �bergeben.
  // Im Gegensatz zu einem Array ist die direkte R�ckgabe eines Struct kein Problem.
  vector3Ds_t result;

  result.x = left->x + right->x;
  result.y = left->y + right->y;
  result.z = left->z + right->z;

  return result;
}

// Von einem 3D-Vektor (Struct) einen anderen 3D-Vektor (Struct) subtrahieren und das Ergebnis liefern.
vector3Ds_t vector3DsSubtract(vector3Ds_t *left, vector3Ds_t *right)
{
  // Der Datentyp "vector3Da_t" ist ein Struct - es wird also immer als Kopie �bergeben.
  // Im Gegensatz zu einem Array ist die direkte R�ckgabe eines Struct kein Problem.
  vector3Ds_t result;

  result.x = left->x - right->x;
  result.y = left->y - right->y;
  result.z = left->z - right->z;

  return result;
}
